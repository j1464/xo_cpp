#include <memory>
#include <windows.h>
#include "xo\game.hpp"
#include <iostream>

LONG handleException(_EXCEPTION_POINTERS* exceptionInfo) {
	if (exceptionInfo) {
		std::cout << "error hapened!" << std::endl;
		// Если мы обработали исключительную ситуацию
		return EXCEPTION_EXECUTE_HANDLER;
	} else {
		// В случае если не можем обработать ситуацию
		// даем шанс отладчику или более вышестоящим 
		// секторам обработать исключение.
		std::cout << "error not happened?" << std::endl;
		return EXCEPTION_CONTINUE_SEARCH;
	}
}

int main() {
	SetUnhandledExceptionFilter(&handleException);

	std::shared_ptr<xo::Game> game(new xo::Game());
	return game->run();
}