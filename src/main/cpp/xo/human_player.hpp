#ifndef JISHKOFF_XO_HUMAN_PLAYER_HPP_20230219_231708
#define JISHKOFF_XO_HUMAN_PLAYER_HPP_20230219_231708

#include "basic_player.hpp"
#include "cursor.hpp"
#include "events.hpp"
#include "input.hpp"

namespace xo {

class HumanPlayer: public BasicPlayer {
private:
	Cursor* _cursor;
	Input* _input;
public:
	HumanPlayer(int id, Cursor* cursor, Input* input);
	~HumanPlayer() override;
	bool handle(const Event& event) override;
	Event input() override;
};
	
}

#endif