#include "game.hpp"
#include <iostream>
#include <string>
#include "log.hpp"
#include "human_player.hpp"
#include "ai_player.hpp"
#include "windows\win_input.hpp"
#include "views\cursor_view.hpp"
#include "views\status_view.hpp"
#include "views\grid_view.hpp"
#include "utils.hpp"

namespace xo {

Game::Game(): 
	_input(nullptr),
	_grid(nullptr),
	_status_text(nullptr),
	_status(nullptr),
	_matrix(nullptr),
	_canvas(nullptr),
	_cursor(nullptr),
	_events(nullptr),
	_players(nullptr),
	_last_i(0),
	_last_j(0),
	_running(true),
	_allow_input(true)
{
	_input = new WinInput();
	_cursor = new Cursor(3);
	_events = new Events();
	_players = new Players();
	_players->add(new HumanPlayer(1, _cursor, _input));
	// TODO: Alexander - Заменить на AI Player объект
	// если мы работаем как игрок в крестики нолики.
	_players->add(new HumanPlayer(2, _cursor, _input));
	_matrix = new Matrix(3, 3);
	_canvas = new Canvas(24);
	_status_text = new StatusText("All ok");
	_status = new StatusView(_status_text);
	_grid = new GridView(_matrix, new CursorView(_cursor));
	_last_i = 0;
	_last_j = 0;
}

Game::~Game() {
	delete _grid;
	delete _status;
	delete _status_text;
	delete _canvas;
	delete _matrix;
	delete _players;
	delete _events;
	delete _cursor;
	delete _input;
}

int Game::run() {
	while (this->isRunning()) {
		this->draw();
		this->input();
		this->update();
	}
	// Костыль в конце игры чтобы отразить то 
	// что показывается в конечном счете 
	this->draw();
	return 0;
}

void Game::draw() {
	_canvas->clear();
	_input->clear();
	_grid->draw(_canvas);
	_status->draw(_canvas);
	_canvas->render();
}

void Game::input() {
	if (_allow_input) {
		_events->push(_players->current()->input());
	} else {
		std::cout << "Input disabled 1" << std::endl;
	}
}

void Game::update() {
	if (_matrix->full()) {
		_status_text->change("Drow");	
		_allow_input = false;
		stop();
	} else if (_matrix->has_filled_line()) {
		_status_text->change("Won player: " + fromCodeToChar(_players->current()->mark()));
		_allow_input = false;
		stop();
	} 
	while (_events->has()) {
		Event e = _events->pop();
		if (!handle(e)) {
			_status_text->change("Unhandled event: " + e.msg);
		}
	}
	
}

bool xPressed(const Event& e) {
	return e.msg == "x";
}

bool ePressed(const Event& e) {
	return e.msg == "e";
}

bool Game::handlePlayers(const Event& event) {
	return _players->current()->handle(event);
}

bool Game::handleX() {
	stop();
	return true;
}

bool Game::handleE() {
	_last_i = _cursor->getX();
	_last_j = _cursor->getY();
	if (_matrix->getAt(_last_j, _last_i) == 0) {
		_matrix->setAt(_last_j, _last_i, _players->current()->mark());
		_players->next();
	} else {
		_status_text->change("Cell is occupied");
	}
	return true;
}

bool Game::handle(const Event& event) {	
	if (xPressed(event)) {
		return handleX();
	} else if (ePressed(event)) {
		return handleE();
	} else {
		return handlePlayers(event);
	}
}

bool Game::isRunning() {
	return _running;
}

void Game::stop() {
	_running = false;
}

}
