#include "event.hpp"

namespace xo {

Event::Event(): Event("") {}

Event::Event(const std::string& msg): msg(msg) {}

Event::Event(const Event& source): Event(source.msg) {}

Event& Event::operator=(const Event& source) {
	if (this != &source) {
		this->msg = source.msg;
	}
	return *this;
}

}