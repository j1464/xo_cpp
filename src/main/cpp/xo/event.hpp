#ifndef JISHKOFF_XO_EVENT_HPP_20230219_232010
#define JISHKOFF_XO_EVENT_HPP_20230219_232010

#include <string>

namespace xo {

struct Event {
	std::string msg;
	Event();
	Event(const std::string& msg);
	Event(const Event& source);
	Event& operator=(const Event& source);
};

}

#endif