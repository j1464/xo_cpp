#ifndef JISHKOFF_AI_PLAYER_HPP_20230223_150501
#define JISHKOFF_AI_PLAYER_HPP_20230223_150501

#include "basic_player.hpp"

namespace xo { 
	
class AIPlayer: public BasicPlayer {
public:
	AIPlayer(int id);
	~AIPlayer() override;
	bool handle(const Event& event) override;
	Event input() override;
};

}

#endif 