#ifndef JISHKOFF_STATUS_TEXT_HPP_20230224_141732
#define JISHKOFF_STATUS_TEXT_HPP_20230224_141732

#include <string>

namespace xo {

class StatusText {
	std::string _text;
public:
	StatusText(const std::string& text);
	void change(const std::string& new_text);
	std::string text();
	int len();
};

}

#endif 