#ifndef JISHKOFF_WINDOWS_WIN_INPUT_HPP_20230219_180302
#define JISHKOFF_WINDOWS_WIN_INPUT_HPP_20230219_180302

#include "..\input.hpp"

namespace xo {

class WinInput: public Input {
public:
	std::string read() override;
	void clear() override;
};

}

#endif