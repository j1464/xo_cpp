#include "win_input.hpp"

#include <conio.h>
#include <windows.h>

namespace xo {

std::string WinInput::read() {
	// TODO: Сделать более красивый и кроссплатформенный
	// ввод с консоли без буфера, напрямую.
	std::string input;
	char c = getch();
	input += c;
	return input;	
}

void WinInput::clear() {
	COORD topLeft  = { 0, 0 };
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO screen;
    DWORD written;

    GetConsoleScreenBufferInfo(console, &screen);
    FillConsoleOutputCharacterA(
        console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
    );
    FillConsoleOutputAttribute(
        console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
        screen.dwSize.X * screen.dwSize.Y, topLeft, &written
    );
    SetConsoleCursorPosition(console, topLeft);
}

}
