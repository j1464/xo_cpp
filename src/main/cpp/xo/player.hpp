#ifndef JISHKOFF_XO_PLAYER_HPP_20230219_231406
#define JISHKOFF_XO_PLAYER_HPP_20230219_231406

#include "event.hpp"
#include "events.hpp"

namespace xo {

struct Player {
	virtual ~Player() = 0;
	virtual int mark() = 0;
	virtual bool handle(const Event& event) = 0;
	virtual Event input() = 0;
};

}

#endif