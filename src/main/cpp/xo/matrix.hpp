#ifndef JISHKOFF_MATRIX_HPP_20230212_165506
#define JISHKOFF_MATRIX_HPP_20230212_165506

namespace xo {

class Matrix {
private:
	int** _cells;
	int _width;
	int _height;
public:
	Matrix(int width, int height);
	~Matrix();
	void setAt(int i, int j, int value);
	int getAt(int i, int j);
	int width();
	int height();
	bool full();
	bool has_filled_line();
	bool has_filled_horizontal();
	bool has_filled_vertical();
	bool has_filled_diagonal();
	bool has_filled_major_diagonal();
	bool has_filled_minor_diagonal();
};

}

#endif