#ifndef JISHKOFF_EVENTS_HPP_20230218_212502
#define JISHKOFF_EVENTS_HPP_20230218_212502

#include <list>
#include "event.hpp"

namespace xo {
	
class Events {
private:
	std::list<Event> _queue;
public:
	Events();
	~Events();
	void push(const Event& event);
	Event pop();
	bool has();
};

}

#endif