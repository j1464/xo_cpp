#ifndef JISHKOFF_XO_GRID_VIEW_HPP_20230219_203804
#define JISHKOFF_XO_GRID_VIEW_HPP_20230219_203804

#include "..\view.hpp"
#include "..\matrix.hpp"
#include "..\cursor.hpp"

namespace xo {

class GridView: public View {
private:
	Matrix* _matrix;
	View* _cursor;
public:
	GridView(Matrix* matrix, View* cursor);
	~GridView();
	void draw(Canvas* canvas) override;
};

}

#endif 