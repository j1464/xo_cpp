#include "status_view.hpp"

namespace xo {
	
StatusView::StatusView(StatusText* text): _text(text) {}

StatusView::~StatusView() {}

void StatusView::draw(Canvas* canvas) {
	// if (_text->len() > 0) {
	// 	return;
	// }
	// TODO:
	int delta = 8;
	int row = delta;
	int text_start_column = 2;
	int len = _text->len();
	std::string s = _text->text();
	canvas->putAt(row, 0, '+');
	canvas->putAt(row, 1, '-');
	for (int i = 2; i < len + 2; i++) {
		canvas->putAt(row, i, '-');
	}
	canvas->putAt(row, len + 2, '-');
	canvas->putAt(row, len + 3, '+');
	row += 1;

	canvas->putAt(row, 0, '|');
	canvas->putAt(row, 1, ' ');
	for (int i = 2; i < len + 2; i++) {
		canvas->putAt(row, i, s[i - 2]);
	}
	canvas->putAt(row, len + 2, ' ');
	canvas->putAt(row, len + 3, '|');

	row +=1;
	canvas->putAt(row, 0, '+');
	canvas->putAt(row, 1, '-');
	for (int i = 2; i < len + 2; i++) {
		canvas->putAt(row, i, '-');
	}
	canvas->putAt(row, len + 2, '-');
	canvas->putAt(row, len + 3, '+');


	// row +=1;
	// canvas->putAt(row, 0, '+');
	// canvas->putAt(row, 1, '+');
	// canvas->putAt(row, 2, '+');
	// canvas->putAt(row, 3, '+');
	// canvas->putAt(row, 4, '+');
}

}