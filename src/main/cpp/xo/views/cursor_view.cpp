#include "cursor_view.hpp"

namespace xo {

CursorView::CursorView(Cursor* cursor): 
	_cursor(cursor) 
{}

CursorView::~CursorView() {
	_cursor = nullptr;
}

void CursorView::draw(Canvas* canvas) {
	// Расчитать правильно смещение относительно текущего 
	// положения курсора
	int delta_x = 0;
	int delta_y = 0;

	delta_x = _cursor->getX() * 2;
	delta_y = _cursor->getY() * 2;

	canvas->putAt(delta_y, delta_x,'#');
	canvas->putAt(delta_y, delta_x + 1,'#');
	canvas->putAt(delta_y, delta_x + 2,'#');
	canvas->putAt(delta_y + 1, delta_x,'#');
	canvas->putAt(delta_y + 1, delta_x + 2,'#');
	canvas->putAt(delta_y + 2, delta_x, '#');
	canvas->putAt(delta_y + 2, delta_x + 1, '#');
	canvas->putAt(delta_y + 2, delta_x + 2, '#');
}

}