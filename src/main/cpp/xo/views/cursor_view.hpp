#ifndef JISHKOFF_XO_CURSOR_VIEW_20230219_203303
#define JISHKOFF_XO_CURSOR_VIEW_20230219_203303

#include "..\view.hpp"
#include "..\cursor.hpp"

namespace xo {

class CursorView: public View {
private:
	Cursor* _cursor;
public:
	CursorView(Cursor* cursor);
	~CursorView() override;
	void draw(Canvas* canvas) override;
};

}

#endif 