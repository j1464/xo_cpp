#ifndef JISHKOFF_STATUS_VIEW_HPP_20230224_141303
#define JISHKOFF_STATUS_VIEW_HPP_20230224_141303

#include "..\view.hpp"
#include "..\canvas.hpp"
#include "..\status_text.hpp"

namespace xo {

class StatusView: public View {
private:
	StatusText* _text;
public:
	StatusView(StatusText* text);
	~StatusView() override;
	void draw(Canvas* canvas) override;
};

}

#endif