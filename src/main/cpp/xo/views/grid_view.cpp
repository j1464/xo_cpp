#include "grid_view.hpp"

#include "..\utils.hpp"

namespace xo {

GridView::GridView(Matrix* matrix, View* cursor):
	_matrix(matrix), 
	_cursor(cursor)
{}

GridView::~GridView() {
	_matrix = nullptr;
	delete _cursor;
}

void GridView::draw(Canvas* canvas) {
	int row = 0;
	int column = 0;

	// first frame
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');

	column = 0;
	row++;

	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++,  fromCodeToChar(_matrix->getAt(0, 0)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++,  fromCodeToChar(_matrix->getAt(0, 1)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++,  fromCodeToChar(_matrix->getAt(0, 2)));
	canvas->putAt(row, column++, '|');

	column = 0;
	row++;

	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');

	column = 0;
	row++;

	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(1, 0)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(1, 1)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(1, 2)));
	canvas->putAt(row, column++, '|');

	column = 0;
	row++;

	// last frame
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');

	column = 0;
	row++;

	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(2, 0)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(2, 1)));
	canvas->putAt(row, column++, '|');
	canvas->putAt(row, column++, fromCodeToChar(_matrix->getAt(2, 2)));
	canvas->putAt(row, column++, '|');

	column = 0;
	row++;

	// last frame
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');
	canvas->putAt(row, column++, '-');
	canvas->putAt(row, column++, '+');

	_cursor->draw(canvas);
}

}