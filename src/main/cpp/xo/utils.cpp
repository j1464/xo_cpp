#include "utils.hpp"

namespace xo {
	
// Конвертация кода в матрице в символьное представление 
// потенциального игрока
char fromCodeToChar(int value) {
	switch(value) {
		case 1: return 'X';
		case 2: return 'O';
	}
	return ' ';
}

}