#include "players.hpp"

#include <iostream>
#include <string>

namespace xo {

Players::Players(): 
	_items(), 
	_position(0)
{}

Players::~Players() {
	for (int i = 0; i < _items.size(); i++) {
		delete _items[i];
	}
}

void Players::add(Player* player) {
	_items.push_back(player);
}

void Players::next() {
	if (_items.size() < 0) {
		return;
	}
	// Jishkoff: 2023-03-05 Так как у нас пока что только два 
	// игрока в игре то изменение позиции будет происходить из
	// двух чисел.
	if (_position == 0) {
		_position = 1;
	} else {
		_position = 0;
	}
}

Player* Players::current() {
	return _items[_position];
}

}