#include "ai_player.hpp"

namespace xo {

AIPlayer::AIPlayer(int id): BasicPlayer(id) {}

AIPlayer::~AIPlayer() {}

bool AIPlayer::handle(const Event& event) {
	return false;
}

Event AIPlayer::input() {
	return Event("");
}


}