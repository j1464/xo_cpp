#include "status_text.hpp"

namespace xo {

StatusText::StatusText(const std::string& text): _text(text) {}

void StatusText::change(const std::string& new_text) {
	_text = new_text;
}

std::string StatusText::text() {
	return _text;
}

int StatusText::len() {
	return _text.size();
}

}