#ifndef JISHKOFF_CURSOR_HPP_20230212_202610
#define JISHKOFF_CURSOR_HPP_20230212_202610

namespace xo {

class Cursor {
private:
    int _x;
    int _y;
    int _size;
public:
    Cursor(int size);
    int getX();
    int getY();
    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();
};

}

#endif