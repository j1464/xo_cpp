#ifndef JISHKOFF_INPUT_HPP_20230219_180001
#define JISHKOFF_INPUT_HPP_20230219_180001

#include <string>

namespace xo {

class Input {
public:
	virtual ~Input() = 0;
	virtual std::string read() = 0;
	virtual void clear() = 0;
};

}

#endif