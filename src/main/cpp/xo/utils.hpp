#ifndef JISHKOFF_UTILS_HPP_20230225_171832
#define JISHKOFF_UTILS_HPP_20230225_171832

namespace xo {

// Конвертация кода в матрице в символьное представление 
// потенциального игрока
char fromCodeToChar(int value);

}

#endif