#ifndef JISHKOFF_GAME_HPP_20230212_162902
#define JISHKOFF_GAME_HPP_20230212_162902

#include "matrix.hpp"
#include "view.hpp"
#include "canvas.hpp"
#include "cursor.hpp"
#include "players.hpp"
#include "events.hpp"
#include "input.hpp"
#include "status_text.hpp"

namespace xo {
    
class Game {
public:
	Game();
	~Game();
	int run();
private:
	Input* _input;
	View* _grid;
	StatusText* _status_text;
	View* _status;
	Matrix* _matrix;
	Canvas* _canvas;
	Cursor* _cursor;
	Events* _events;
	Players* _players;
	int _last_i;
	int _last_j;
	bool _running;
	bool _allow_input;
	void draw();
	void input();
	void update();
	bool handle(const Event& event);
	bool handleX();
	bool handleE();
	bool handlePlayers(const Event& event);
	bool isRunning();
	void stop();
};

}

#endif