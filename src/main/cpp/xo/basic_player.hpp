#ifndef JISHKOFF_XO_BASIC_PLAYER_HPP_20230305
#define JISHKOFF_XO_BASIC_PLAYER_HPP_20230305

#include "player.hpp"

namespace xo {

class BasicPlayer: public Player {
protected:
	int _id;
public:
	BasicPlayer(int id);
	int mark() override;
};

}

#endif