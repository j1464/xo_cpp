#ifndef JISHKOFF_VIEW_HPP_20230212_163103
#define JISHKOFF_VIEW_HPP_20230212_163103

#include "canvas.hpp"

namespace xo {

struct View {
public:
	virtual ~View() = 0;
	virtual void draw(Canvas* canvas) = 0;
};

}

#endif