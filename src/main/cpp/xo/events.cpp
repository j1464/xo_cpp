#include "events.hpp"

namespace xo {

Events::Events() {}

Events::~Events() {}

void Events::push(const Event& event) {
	_queue.push_back(event);
}

Event Events::pop() {
	Event e = _queue.front();
	_queue.pop_front();
	return e;
}

bool Events::has() {
	return _queue.size() > 0;
}


}