#include "cursor.hpp"

namespace xo {

Cursor::Cursor(int size):
    _x(0), _y(0), _size(size)
{}

int Cursor::getX() {
    return _x;
}

int Cursor::getY() {
    return _y;
}

void Cursor::moveUp() {
    if (_y > 0) {
        _y -= 1;
    }
}

void Cursor::moveDown() {
    if (_y < _size - 1) {
        _y += 1;
    }
}

void Cursor::moveRight() {
    if (_x < _size - 1) {
        _x += 1;
    }
}

void Cursor::moveLeft() {
    if (_x > 0) {
        _x -= 1;
    }
}

}