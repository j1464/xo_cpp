#ifndef JISHKOFF_CANVAS_HPP_20230212_174607
#define JISHKOFF_CANVAS_HPP_20230212_174607

namespace xo {

class Canvas {
private:
    char** _buffer;
    int _size;
    char _clear_char;
    char* _render_buffer;
public:
    Canvas(int size);
    ~Canvas();
    void putAt(int i, int j, char item);
    void clear(); 
    void render();
};

}

#endif