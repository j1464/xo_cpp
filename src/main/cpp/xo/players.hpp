#ifndef JISHKOFF_PLAYERS_HPP_20230212_205412
#define JISHKOFF_PLAYERS_HPP_20230212_205412

#include <vector>

#include "player.hpp"
#include "events.hpp"

namespace xo {

class Players {
private:
	int _position;
	std::vector<Player*> _items;
public:
	Players();
	~Players();
	void add(Player* player);
	void next();
	Player* current();
};

}

#endif