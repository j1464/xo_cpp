#include "human_player.hpp"

namespace xo {

HumanPlayer::HumanPlayer(int id, Cursor* cursor, Input* input): 
	BasicPlayer(id), 
	_cursor(cursor),
	_input(input)
{}

HumanPlayer::~HumanPlayer() {
	_cursor = nullptr;
	_input = nullptr;
}

bool HumanPlayer::handle(const Event& event) {
	if (event.msg == "w") {
		_cursor->moveUp();
		return true;
	} else if (event.msg == "s") {
		_cursor->moveDown();
		return true;
	} else if (event.msg == "d") {
		_cursor->moveRight();
		return true;
	} else if (event.msg == "a") {
		_cursor->moveLeft();
		return true;
	} else {
		return false;
	}
}

Event HumanPlayer::input() {
	return Event(_input->read());
}

}