#include "matrix.hpp"

namespace xo {

Matrix::Matrix(int width, int height):
	_cells(nullptr),
	_width(width),
	_height(height)
{
	_cells = new int*[_height];
	for (int i = 0; i < _height; i++) {
		_cells[i] = new int[_width];
		for (int j = 0; j < _width; j++) {
			_cells[i][j] = 0;
		}
	}
}

Matrix::~Matrix() {
	for (int i = 0; i < _height; i++) {
		delete[] _cells[i];
		_cells[i] = nullptr;
	}
	delete[] _cells;
	_cells = nullptr;
}

void Matrix::setAt(int i, int j, int value) {
	_cells[i][j] = value;
}

int Matrix::getAt(int i, int j) {
	return _cells[i][j];
}

int Matrix::width() {
	return _width;
}

int Matrix::height() {
	return _height;
}

bool Matrix::full() {
	for (int i = 0; i < _width; i++) {
		for (int j = 0; j < _height; j++) {
			if (_cells[i][j] <= 0) 
				return false;
		}	
	}
	return true;
}

bool Matrix::has_filled_line() {
	return has_filled_horizontal() || has_filled_vertical() || has_filled_diagonal();
}

bool Matrix::has_filled_horizontal() {
	int elem = -1;
	for (int i = 0; i < _height; i++) {
		elem = _cells[i][0];
		if (elem == 0) {
			continue;
		}
		for (int j = 1; j < _width; j++) {
			if (elem != _cells[i][j]) {
				return false;
			}
		}
	}
	if (elem == 0) {
		return false;
	} else {
		return true;
	}
}

bool Matrix::has_filled_vertical() {
	int elem = -1;
	for (int i = 0; i < _height; i++) {
		elem = _cells[0][i];
		if (elem == 0) {
			continue;
		}
		for (int j = 1; j < _width; j++) {
			if (elem != _cells[j][i]) {
				return false;
			}
		}
	}
	if (elem == 0) {
		return false;
	} else {
		return true;
	}
}

bool Matrix::has_filled_diagonal() {
	return has_filled_major_diagonal() || has_filled_minor_diagonal();
}

bool Matrix::has_filled_major_diagonal() {
	int elem = _cells[0][0];
	if (elem == 0) {
		return false;
	}
	for (int i = 1; i < _width; i++) {
		if (elem != _cells[i][i]) {
			return false;
		}
	}
	return true;
}

bool Matrix::has_filled_minor_diagonal() {
	int elem = _cells[0][_width - 1];
	if (elem == 0) {
		return false;
	}
	for (int i = 1; i < _width; i++) {
		if (elem != _cells[i][_width - 1 - i]) {
			return false;
		}
	}
	return true;
}

}