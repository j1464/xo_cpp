#include "canvas.hpp"

#include <iostream>
#include <cstring>

namespace xo {

Canvas::Canvas(int size): 
    _buffer(nullptr), 
    _size(size), 
    _clear_char('.'),
    _render_buffer(nullptr)
{
    _buffer = new char*[_size];
    for (int i = 0; i < _size; i++) {
        _buffer[i] = new char[_size + 1];
        memset(_buffer[i], _clear_char, _size);
        _buffer[i][_size] = '\0';
    }
    int sz = _size * (_size + 1);
    _render_buffer = new char[sz];
}

Canvas::~Canvas() {
    delete[] _render_buffer;
    for (int i = 0; i < _size; i++) {
        delete[] _buffer[i];
    }
    delete[] _buffer;
}

void Canvas::putAt(int i, int j, char item) {
    _buffer[i][j] = item;
}

void Canvas::clear() {
    for (int i = 0; i < _size; i++) {
        memset(_buffer[i], _clear_char, _size);
    }
} 

void Canvas::render() {
    int size = _size * (_size + 1);
    memset(_render_buffer, '\0', size);
    _render_buffer = strcpy(_render_buffer, _buffer[0]);
    _render_buffer = strcat(_render_buffer, "\n");
    for (int i = 1; i < _size; i++) {
        _render_buffer = strcat(_render_buffer, _buffer[i]);
        _render_buffer = strcat(_render_buffer, "\n");
    }
    std::cout << _render_buffer << std::endl;
}

}