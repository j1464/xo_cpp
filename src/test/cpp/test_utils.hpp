#ifndef JISKOFF_TEST_UTILS_HPP_20230301_213501
#define JISKOFF_TEST_UTILS_HPP_20230301_213501

namespace xo {

using BoolFunc = bool (*)();
using VoidFunc = void (*)();

void it(const char* text, bool expression);
void it(const char* text, BoolFunc func);
void describe(const char* text, VoidFunc func);

}

#endif