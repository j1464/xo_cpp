#include <iostream>
#include <string>
#include "test_utils.hpp"
#include <xo/cursor.hpp>
#include <xo/matrix.hpp>

int main(int argc, char** argv) {
	using xo::describe;
	using xo::it;
	describe("Testing Cursor", [](){
		it("Should getX at start is 0", 
			[](){ xo::Cursor(3).getX() == 0; }
		);
		it("Should getY at start is 0", 
			[](){ xo::Cursor(3).getY() == 0; }
		);
		it("Y location after move down changed to 1", 
			[]() {
				xo::Cursor c(3);
				int prevY = c.getY();
				c.moveDown();
				return (prevY != c.getY()) && (c.getY() == 1);
			}
		);
		it("Y location after move up not changed",
			[]() {
				xo::Cursor c(3);
				int prevY = c.getY();
				c.moveUp();
				return (prevY == c.getY()) && (c.getY() == 0);
			}
		);
		it("X location after move right changed to 1", 
			[]() {
				xo::Cursor c(3);
				int prevX = c.getX();
				c.moveRight();
				return (prevX != c.getX()) && (c.getX() == 1);
			}
		);
		it("X location after move left X not changed", 
			[]() {
				xo::Cursor c(3);
				int prevX = c.getX();
				c.moveLeft();
				return (prevX == c.getX()) && (c.getX() == 0);
			}
		);
	});
	describe("Testing Matrix", [](){
		it("Getting element at 0 0 works well",
			[]() {
				bool allValid = true;
				xo::Matrix m(3, 3);
				for (int i = 0; i < 3; i++){
					for (int j = 0; j < 3; j++) {
						m.setAt(i,j,1);
						allValid = allValid && m.getAt(i, j) == 1;
					}
				}
				return allValid;
			}
		);
		it("Detect filled row successfull",
			[]() {
				xo::Matrix m(3, 3);
				m.setAt(1,0, 1);
				m.setAt(1,1, 1);
				m.setAt(1,2, 1);
				return m.has_filled_horizontal();
			}
		);
		it("Detect filled column successfull",
			[]() {
				xo::Matrix m(3, 3);
				m.setAt(0, 1, 1);
				m.setAt(1, 1, 1);
				m.setAt(2, 1, 1);
				return m.has_filled_vertical();
			}
		);
		it("Diagonal major line has found correctly in matrix",
			[]() {
				xo::Matrix m(3, 3);
				m.setAt(0,0, 1);
				m.setAt(1,1, 1);
				m.setAt(2,2, 1);
				return m.has_filled_major_diagonal();
			}
		);
		it("Diagonal minor line has found correctly in matrix",
			[]() {
				xo::Matrix m(3, 3);
				m.setAt(0,2, 1);
				m.setAt(1,1, 1);
				m.setAt(2,0, 1);
				return m.has_filled_minor_diagonal();
			}
		);
		it("Detecting filled matrix works", 
			[](){
				const int N = 3;
				xo::Matrix m(N, N);
				for (int i = 0; i < N; i++) {
					for (int j = 0; j < N; j++) {
						m.setAt(i, j, 1);
					}	
				}
				return m.full();
			}
		);
		it("Detecting line with empty matrix should fail", 
			[](){
				const int N = 3;
				xo::Matrix m(N, N);
				return !m.has_filled_line();
			}
		);
	});
	return 0;
}