#include "test_utils.hpp"

#include <iostream>

namespace xo {

void it(const char* text, bool expression) {
	std::cout << std::string((expression) ? "ok" : "bad") << '\t'<< text << std::endl;
}

void it(const char* text, BoolFunc func) {
	it(text, func());
}

void describe(const char* text, VoidFunc func) {
	std::cout << text << std::endl;
	func();
}


}

